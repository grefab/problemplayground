package sudoku

import "fmt"

type Board struct {
	tiles [81]int
}

// we assume a string containing numbers row-wise.
// ' ' or '-' are treated as empty. all others characters are omitted.
// panics if not exactly 81 tiles have been found
func ParseFromString(s string) Board {
	board := Board{}
	tileIdx := 0
	for _, c := range s {
		switch {
		case c == ' ' || c == '-':
			tileIdx++
		case c >= '1' && c <= '9':
			board.tiles[tileIdx] = int(c - '0')
			tileIdx++
		}
	}
	if tileIdx != 81 {
		panic(fmt.Sprintf("could not parse 81 tiles, got %v", tileIdx))
	}
	return board
}

// prints board row-wise
func (b *Board) ToString() string {
	s := ""
	for y := 0; y < 9; y++ {
		for x := 0; x < 9; x++ {
			// convert tile at x, y to character
			idx := y*9 + x
			if b.tiles[idx] != 0 {
				s += fmt.Sprintf("%v", b.tiles[idx])
			} else {
				s += " "
			}
		}
		s += "\n"
	}
	return s
}

func (b *Board) Solve() Board {
	return *solveRec(*b, 0)
}

func solveRec(board Board, startIdx int) *Board {
	// for each tile find first tile that does not have a number yet
	for idx := startIdx; idx < 9*9; idx++ {
		if board.tiles[idx] == 0 {
			// iterate over all possible numbers
			for i := 1; i <= 9; i++ {
				board.tiles[idx] = i
				if board.isValid() {
					// if valid, launch new process with updated board
					r := solveRec(board, idx+1)
					if r != nil {
						return r
					}
				}
			}
			return nil
		}
	}
	return &board
}

func (b *Board) isValid() bool {
	verifyRow := func(y int) bool {
		hit := make(map[int]bool)
		for x := 0; x < 9; x++ {
			idx := y*9 + x
			number := b.tiles[idx]
			if number > 0 {
				if hit[number] {
					return false
				}
				hit[number] = true
			}
		}
		return true
	}
	verifyColumn := func(x int) bool {
		hit := make(map[int]bool)
		for y := 0; y < 9; y++ {
			idx := y*9 + x
			number := b.tiles[idx]
			if number > 0 {
				if hit[number] {
					return false
				}
				hit[number] = true
			}
		}
		return true
	}
	verifySquare := func(x, y int) bool {
		hit := make(map[int]bool)
		squareTop := y - y%3
		squareBottom := squareTop + 3
		squareLeft := x - x%3
		squareRight := squareLeft + 3

		for yi := squareTop; yi < squareBottom; yi++ {
			for xi := squareLeft; xi < squareRight; xi++ {
				idx := yi*9 + xi
				number := b.tiles[idx]
				if number > 0 {
					if hit[number] {
						return false
					}
					hit[number] = true
				}
			}
		}
		return true
	}

	for y := 0; y < 9; y++ {
		if !verifyRow(y) {
			return false
		}
	}

	for x := 0; x < 9; x++ {
		if !verifyColumn(x) {
			return false
		}
	}

	return verifySquare(0, 0) &&
		verifySquare(3, 0) &&
		verifySquare(6, 0) &&
		verifySquare(0, 3) &&
		verifySquare(3, 3) &&
		verifySquare(6, 3) &&
		verifySquare(0, 6) &&
		verifySquare(3, 6) &&
		verifySquare(6, 6)
}
