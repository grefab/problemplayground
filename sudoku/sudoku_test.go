package sudoku

import (
	"testing"
)

func TestParseFromString(t *testing.T) {
	difficultToParse := makeParseTestBoard()
	easyToParse := makeTestBoard()
	if difficultToParse != easyToParse {
		t.Errorf("parsing failed\nexpected:\n%v\nparsed:\n%v", easyToParse.ToString(), difficultToParse.ToString())
	}
}

func TestSolve(t *testing.T) {
	problem := makeEasyTestBoard()
	solution := makeSolvedBoard()
	solved := problem.Solve()
	if solved != solution {
		t.Errorf("solving did not produce expected solution.\nexpected:\n%v\nsolved:\n%v", solution.ToString(), solved.ToString())
	}
}

func TestSolve2(t *testing.T) {
	problem := makeTestBoard()
	solution := makeSolvedBoard()
	solved := problem.Solve()
	if solved != solution {
		t.Errorf("solving did not produce expected solution.\nexpected:\n%v\nsolved:\n%v", solution.ToString(), solved.ToString())
	}
}

func makeTestBoard() Board {
	// see: https://en.wikipedia.org/wiki/Sudoku#/media/File:Sudoku_Puzzle_by_L2G-20050714_standardized_layout.svg
	s := "53  7    " +
		"6  195   " +
		" 98    6 " +
		"8   6   3" +
		"4  8 3  1" +
		"7   2   6" +
		" 6    28 " +
		"   419  5" +
		"    8  79"

	return ParseFromString(s)
}

func makeParseTestBoard() Board {
	s := "53  7    \n" +
		"6  195   ++" +
		" 98    6 " +
		"8 XXX  6   3" +
		"4  8 3  1" +
		"7   2   6" +
		" 6    28 " +
		"   419  5" +
		"  ZZZ  8  79"

	return ParseFromString(s)
}

func makeEasyTestBoard() Board {
	// see: https://en.wikipedia.org/wiki/Sudoku#/media/File:Sudoku_Puzzle_by_L2G-20050714_standardized_layout.svg
	s := "534678912" +
		"672195348" +
		"198342567" +
		"859761423" +
		"426853791" +
		"713924856" +
		"961537284" +
		"287419635" +
		"34528617 "

	return ParseFromString(s)
}

func makeSolvedBoard() Board {
	// see: https://en.wikipedia.org/wiki/Sudoku#/media/File:Sudoku_Puzzle_by_L2G-20050714_solution_standardized_layout.svg
	s := "534678912" +
		"672195348" +
		"198342567" +
		"859761423" +
		"426853791" +
		"713924856" +
		"961537284" +
		"287419635" +
		"345286179"
	return ParseFromString(s)
}
