package xipimax

import (
	"testing"
)

func TestMakeElements(t *testing.T) {
	correctElements := makeManuallySortedElements()
	createdElements := makeElements()

	if !equal(createdElements, correctElements) {
		t.Errorf("creating elements failed")
	}
}

func makeManuallySortedElements() Elements {
	return Elements{
		Element{
			x: 1,
			p: -100,
		},
		Element{
			x: 2,
			p: 100,
		},
		Element{
			x: 3,
			p: -50,
		},
		Element{
			x: 5,
			p: 50,
		},
		Element{
			x: 8,
			p: 0,
		},
	}
}

func makeElements() Elements {
	return MakeElements(
		Element{
			x: 8,
			p: 0,
		},
		Element{
			x: 5,
			p: 50,
		},
		Element{
			x: 2,
			p: 100,
		},
		Element{
			x: 3,
			p: -50,
		},
		Element{
			x: 1,
			p: -100,
		},
	)
}

func makeElementsP0() Elements {
	return MakeElements(
		Element{
			x: 1,
			p: 0,
		},
		Element{
			x: 2,
			p: 0,
		},
		Element{
			x: 3,
			p: 0,
		},
		Element{
			x: 5,
			p: 0,
		},
		Element{
			x: 8,
			p: 0,
		},
	)
}

func equal(a, b []Element) bool {
	if len(a) != len(b) {
		return false
	}
	for i, v := range a {
		if v != b[i] {
			return false
		}
	}
	return true
}

func TestElements_Solve(t *testing.T) {
	tests := []struct {
		name     string
		elements Elements
		wantI    int
		wantJ    int
	}{
		{
			name:     "easy",
			elements: makeElements(),
			wantI:    1,
			wantJ:    3,
		},
		{
			name:     "harder",
			elements: makeElementsP0(),
			wantI:    0,
			wantJ:    4,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			gotI, gotJ := tt.elements.Solve()
			if gotI != tt.wantI {
				t.Errorf("Solve() gotI = %v, want %v", gotI, tt.wantI)
			}
			if gotJ != tt.wantJ {
				t.Errorf("Solve() gotJ = %v, want %v", gotJ, tt.wantJ)
			}
		})
	}
}
