package xipimax

import (
	"sort"
)

type Element struct {
	x int // should be always positive
	p int
}

type Elements []Element

func MakeElements(p ...Element) Elements {
	f := Elements{}

	for _, x := range p {
		f = append(f, x)
	}

	sort.Slice(f, func(i, j int) bool {
		return f[i].x < f[j].x
	})

	return f
}

func f(a, b Element) int {
	return a.p + b.p + abs(a.x-b.x)
}

func abs(x int) int {
	if x < 0 {
		return -x
	}
	return x
}

func (elements Elements) Solve() (i, j int) {
	// fixate i
	i = 0

	// find corresponding j where f(i, j) -> max
	{
		maxJ := -1
		maxV := -1
		for j = i + 1; j < len(elements); j++ {
			v := f(elements[i], elements[j])
			if v > maxV {
				maxV = v
				maxJ = j
			}
		}
		j = maxJ
	}

	// for that j find i, such that i != j and f(i, j) -> max
	{
		maxI := -1
		maxV := -1
		for i = 0; i < len(elements); i++ {
			if i == j {
				continue
			}

			v := f(elements[i], elements[j])
			if v > maxV {
				maxV = v
				maxI = i
			}
		}
		i = maxI
	}

	if i < j {
		return i, j
	} else {
		return j, i
	}
}
